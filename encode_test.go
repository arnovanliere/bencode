package bencode

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEncode(t *testing.T) {
	t.Run("Encode list", func(t *testing.T) {
		res, err := Encode([]interface{}{
			[]interface{}{
				[]byte("spam"),
				int64(42),
			},
		})
		assert.NoError(t, err, "error encoding list: %s", err)
		assert.Equal(t, "l4:spami42ee", res)
	})
	t.Run("Encode int", func(t *testing.T) {
		res, err := Encode([]interface{}{int64(42)})
		assert.NoError(t, err, "error encoding int: %s", err)
		assert.Equal(t, "i42e", res)
	})
	t.Run("Encode negative int", func(t *testing.T) {
		res, err := Encode([]interface{}{int64(-42)})
		assert.NoError(t, err, "error encoding negative int: %s", err)
		assert.Equal(t, "i-42e", res)
	})
	t.Run("Encode byte-string", func(t *testing.T) {
		res, err := Encode([]interface{}{[]byte("spam")})
		assert.NoError(t, err, "error encoding byte-string: %s", err)
		assert.Equal(t, "4:spam", res)
	})
	t.Run("Encode dictionary", func(t *testing.T) {
		res, err := Encode([]interface{}{
			map[string]interface{}{
				"bar": []byte("spam"),
			},
		})
		assert.NoError(t, err, "error encoding dictionary: %s", err)
		assert.Equal(t, "d3:bar4:spame", res)
	})
	t.Run("Encode bencode string", func(t *testing.T) {
		res, err := Encode([]interface{}{
			[]interface{}{
				[]byte("spam"),
				int64(42),
			},
			int64(42),
			int64(-42),
			[]byte("spam"),
			map[string]interface{}{
				"bar": []byte("spam"),
			},
		})
		assert.NoError(t, err, "error encoding bencode string: %s", err)
		assert.Equal(t, "l4:spami42eei42ei-42e4:spamd3:bar4:spame", res)
	})
}
