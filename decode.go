package bencode

import (
	"errors"
	"fmt"
	"strconv"
	"unicode/utf8"
)

// Decode takes in a string and decodes it according to the bencode standard.
// It returns a slice with all the encoded values in "data" and an error
// if an invalidly encoded string is provided to decode.
func Decode(data string) ([]interface{}, error) {
	var decoded []interface{}
	_, err := decode(data, &decoded, false)
	if err != nil {
		return nil, err
	}
	return decoded, nil
}

// decode is a function which recursively decodes a string encoded according to
// the bencode standard. All decoded values are saved in the "decoded" slice.
// An error is returned if the string is not encoded according to the bencode standard.
// If "list" is true, the decoding will stop on "e".
func decode(data string, decoded *[]interface{}, list bool) (string, error) {
	var err error
	var parsed interface{}

	// Nothing left to decode
	if len(data) == 0 {
		return data, nil
	}

	// If this is decoded as list, it should end with "e"
	if list && len(data) == 0 {
		return "", errors.New("list ended without 'e'")
	} else if list && data[0] == 'e' {
		return data, nil
	}

	if data[0] == 'd' {
		// Parse dictionary
		parsed, err = decodeDictionary(&data)
	} else if data[0] == 'i' {
		// Parse integer
		parsed, err = decodeInt(&data)
	} else if data[0] == 'l' {
		// Parse list
		parsed, err = decodeList(&data)
	} else if data[0] >= '0' && data[0] <= '9' {
		// Parse byte string
		parsed, err = decodeByteString(&data)
	} else {
		// Invalid character found
		return "", errors.New(fmt.Sprintf("invalid character: %s", string(data[0])))
	}

	if err != nil {
		return "", err
	}

	// Append parsed value
	if parsed != nil {
		*decoded = append(*decoded, parsed)
	}

	return decode(data, decoded, list)
}

// decodeInt parses the beginning of the string "data" as integer, encoded according to the
// bencode standard. The integer is removed from the string if the decoding was successful.
// The decoded integer and an error are returned.
func decodeInt(data *string) (int64, error) {
	intStr := ""
	var i int

	// Start at 1 to skip the "i" in "data"
	for i = 1; i < len(*data); i++ {
		// "e" indicates end of integer
		if (*data)[i] == 'e' {
			i++
			break
		}
		if ((*data)[i] >= '0' && (*data)[i] <= '9') || (i == 1 && (*data)[i] == '-') {
			// Append integer to string
			intStr += string((*data)[i])
		} else {
			// Invalid character found
			return 0, errors.New(fmt.Sprintf("invalid character: %s", string((*data)[i])))
		}
	}

	// Remove integer from the data
	*data = (*data)[i:]

	// Return result of integer conversion
	return strconv.ParseInt(intStr, 10, 64)

}

// decodeByteString parses the beginning of the string "data" as byte-string, encoded according to the
// bencode standard. The byte-string is removed from the string if the decoding was successful.
// The decoded byte-string and an error are returned.
func decodeByteString(data *string) ([]byte, error) {
	lengthStr := ""
	var i int

	// Read the length of the byte-string until ":"
	for i = 0; i < len(*data); i++ {
		// ":" indicates the end of the length
		if (*data)[i] == ':' {
			i++
			break
		}
		if (*data)[i] >= '0' && (*data)[i] <= '9' {
			// Append integer to string
			lengthStr += string((*data)[i])
		} else {
			// Invalid character found
			return nil, errors.New(fmt.Sprintf("invalid character: %s", string((*data)[i])))
		}
	}

	// Convert string to integer
	parsedInt, err := strconv.ParseInt(lengthStr, 10, 64)
	if err != nil {
		return nil, err
	}

	// Not enough of the data left to get a string of the specified length
	if parsedInt > int64(len(*data)-i) {
		return nil, errors.New("byte-string length not valid")
	}

	// Extract byte-string
	bytes := (*data)[i : int64(i)+parsedInt]

	// Remove byte-string from the data
	*data = (*data)[int64(i)+parsedInt:]

	return []byte(bytes), nil
}

// decodeList parses the beginning of the string "data" as list, encoded according to the
// bencode standard. The list is removed from the string if the decoding was successful.
// The decoded list and an error are returned.
func decodeList(data *string) ([]interface{}, error) {
	var list []interface{}

	// Decode list. Start at 1 to skip "l".
	remaining, err := decode((*data)[1:], &list, true)
	if err != nil {
		return nil, err
	}

	// Remove the list from the data
	*data = remaining[1:]

	return list, nil
}

// decodeDictionary parses the beginning of the string "data" as dictionary, encoded according to the
// bencode standard. The dictionary is removed from the string if the decoding was successful.
// The decoded dictionary and an error are returned.
func decodeDictionary(data *string) (map[string]interface{}, error) {
	dictionary := map[string]interface{}{}

	// Decode as list
	list, err := decodeList(data)
	if err != nil {
		return nil, err
	}

	// There should be as much keys as values
	if len(list)%2 != 0 {
		return nil, errors.New("more keys than values")
	}

	// Set key/values in dictionary
	for i := 0; i < len(list); i += 2 {
		bytes, ok := (list[i]).([]byte)
		if !ok {
			return nil, errors.New("key is not a valid byte-array")
		}
		if !utf8.Valid(bytes) {
			return nil, errors.New("key is not a valid UTF-8 string")
		}
		dictionary[string(bytes)] = list[i+1]
	}

	return dictionary, nil
}
