package bencode

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDecode(t *testing.T) {
	t.Run("Decode list", func(t *testing.T) {
		res, err := Decode("l4:spami42ee")
		assert.NoError(t, err, "error decoding list: %s", err)
		assert.Equal(t, 1, len(res), "length decoding: %v != 1", len(res))
		assert.Equal(t, []interface{}{[]byte("spam"), int64(42)}, res[0])
	})
	t.Run("Decode int", func(t *testing.T) {
		res, err := Decode("i42e")
		assert.NoError(t, err, "error decoding int: %s", err)
		assert.Equal(t, 1, len(res), "length decoding: %v != 1", len(res))
		assert.Equal(t, int64(42), res[0])
	})
	t.Run("Decode negative int", func(t *testing.T) {
		res, err := Decode("i-42e")
		assert.NoError(t, err, "error decoding int: %s", err)
		assert.Equal(t, 1, len(res), "length decoding: %v != 1", len(res))
		assert.Equal(t, int64(-42), res[0])
	})
	t.Run("Decode byte-string", func(t *testing.T) {
		res, err := Decode("4:spam")
		assert.NoError(t, err, "error decoding byte-string: %s", err)
		assert.Equal(t, 1, len(res), "length decoding: %v != 1", len(res))
		assert.Equal(t, []byte("spam"), res[0])
	})
	t.Run("Decode dictionary", func(t *testing.T) {
		res, err := Decode("d3:bar4:spame")
		assert.NoError(t, err, "error decoding dictionary: %s", err)
		assert.Equal(t, 1, len(res), "length decoding: %v != 1", len(res))
		assert.Equal(t, map[string]interface{}{"bar": []byte("spam")}, res[0])
	})
	t.Run("Decode bencode string", func(t *testing.T) {
		res, err := Decode("l4:spami42eei42ei-42e4:spamd3:bar4:spame")
		assert.NoError(t, err, "error decoding bencode string: %s", err)
		assert.Equal(t, 5, len(res), "length decoding: %v != 5", len(res))
		assert.Equal(t, []interface{}{
			[]interface{}{
				[]byte("spam"),
				int64(42),
			},
			int64(42),
			int64(-42),
			[]byte("spam"),
			map[string]interface{}{
				"bar": []byte("spam"),
			},
		}, res)
	})
}
