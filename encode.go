package bencode

import (
	"errors"
	"fmt"
	"strconv"
)

// Encode encodes a list of values according to the bencode-standard.
// The result and and error are returned.
func Encode(data []interface{}) (string, error) {
	res := ""
	for _, el := range data {
		encoded, err := encode(el)
		if err != nil {
			return "", err
		}
		res += encoded
	}
	return res, nil
}

// encode encodes a single value according to the bencode-standard.
// It returns the result and an error.
func encode(data interface{}) (string, error) {
	switch v := data.(type) {
	case int64:
		return encodeInt(v), nil
	case []byte:
		return encodeByteString(v), nil
	case []interface{}:
		return encodeList(v)
	case map[string]interface{}:
		return encodeDictionary(v)
	default:
		return "", errors.New(fmt.Sprintf("invalid type found: %v", v))
	}
}

// encodeInt encodes an integer according to the bencode standard.
func encodeInt(data int64) string {
	return "i" + strconv.FormatInt(data, 10) + "e"
}

// encodeByteString encodes a byte-string according to the bencode standard.
func encodeByteString(data []byte) string {
	return strconv.Itoa(len(string(data))) + ":" + string(data)
}

// encodeList encodes a list according to the bencode standard.
// It returns the result and an error.
func encodeList(data []interface{}) (string, error) {
	res, err := Encode(data)
	if err != nil {
		return "", err
	}
	return "l" + res + "e", nil
}

// encodeDictionary encodes a dictionary according to the bencode standard.
// It returns the result and an error.
func encodeDictionary(data map[string]interface{}) (string, error) {
	res := "d"
	for key, value := range data {
		val, err := encode(value)
		if err != nil {
			return "", err
		}
		res += strconv.Itoa(len(key)) + ":" + key + val
	}
	return res + "e", nil
}
